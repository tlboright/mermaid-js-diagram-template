#!/bin/bash

# directory containing files to be renamed
DIR="files"

DIAGRAM_DIR="./diagrams"

# file extension to be changed to

for file in $DIR/*
do
    # get file name without extension
    name=$(basename "${file%.*}")

    # run mermaid
    mmdc -i "$file" -o "$DIAGRAM_DIR/$name.png"
done
